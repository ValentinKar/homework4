<?php 
mb_internal_encoding('utf-8');  //позволяет использовать все ф-ции для кириллицы
error_reporting(E_ALL);        //вывести на экран все ошибки

if ($_GET['city'] !== '1') { 
	$string_id = (string)$_GET['city'];  //id города, выбранного пользователем
}
else { 
	$string_id = '524901';    //id Москвы
}
$string_1 = "http://api.openweathermap.org/data/2.5/weather?id=".$string_id."&units=metric&APPID=28b9749dfd94832eb666258ac68236a5";
//запрос погоды по api с сайта openweathermap.org

$content = file_get_contents($string_1);  //выполняю запрос с сайта и передаю переменной
//var_dump($string_1);
$array = json_decode($content, true);  //декодирую полученные данные с сайта (формат json) в массив 
$main = $array['main'];   //в массив $main записываю данные о погоде
$city = file_get_contents("city.list_1.json");  //запрашиваю файл city.list_1.json в нем содержатся названия и $id городов
$city_1 = json_decode($city, true);             //декодирую запрошенные данные о городах в массив 
?>

<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Weather</title>
<style>
body {
  font-family: sans-serif;
  font-size: 15px;
}
.p_1 {
  line-height: 22px;
  margin-bottom: 30px;
}
dt {
  float: left;
  margin-left: 20px;
  margin-bottom: 10px;
}
dd {
  margin-left: 250px;
  margin-bottom: 10px;
}
</style>

</head>
<body>
<h1>Weather in (погода в) <?php echo $array['name']; ?>!</h1>
<form action="index.php?id=" method="get" enctype="multipart/form-data">
<label>Я живу в другом городе: 
<select size="1" name="city">
<?php foreach ($city_1 as $key => $city_D) :  ?>
<option value='<?php echo $city_D["id"]; ?>'><?php echo $city_D["name"].'_'.$city_D["country"]; ?></option>
 <?php endforeach; ?>
</select>
</label>
<input type="submit" value="Выбрать">
</form>
<br/><br/>

  <img src = "http://openweathermap.org/themes/openweathermap/assets/vendor/mosaic/img/base_owm.jpg" />

<dl>
   <dt>
    Температура:
   </dt>
   <dd>
    <?php echo $main['temp']; ?> град. Цельсия
   </dd>

   <dt>
   Давление:
   </dt>
   <dd>
    <?php echo $main['pressure']; ?> милибар (гектопаскалей)
   </dd>

   <dt>
    влажность:
   </dt>
   <dd>
    <?php echo $main['humidity']; ?> %
   </dd>

    <dt>
    минимальная температура:
   </dt>
   <dd>
    <?php echo $main['temp_min']; ?> град. Цельсия
   </dd>

    <dt>
    максимальная температура:
   </dt>
   <dd>
    <?php echo $main['temp_max']; ?> град. Цельсия
   </dd>
</dl>
<script src='http://openweathermap.org/themes/openweathermap/assets/vendor/owm/js/d3.min.js'></script><div id='openweathermap-widget'></div>
                    <script type='text/javascript'>
                    window.myWidgetParam = {
                        id: 11,
                        cityid: <?php echo $string_id;//2022572 ?>,
                        appid: '28b9749dfd94832eb666258ac68236a5',
                        units: 'metric',
                        containerid: 'openweathermap-widget',                        
                    };
                    (function() {
                        var script = document.createElement('script');
                        script.type = 'text/javascript';
                        script.async = true;
                        script.src = 'http://openweathermap.org/themes/openweathermap/assets/vendor/owm/js/weather-widget-generator.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(script, s);
                    })();
                  </script>
</body>
</html>